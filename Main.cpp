//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Main.h"
#include "TDocument.h"
#include "Report.h"
#include "Statystyka.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------

TForm1 *Form1;
//---------------------------------------------------------------------------

__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
	new TDocument(PageControl1);
}

//---------------------------------------------------------------------------

void __fastcall TForm1::Otwrz1Click(TObject *Sender)
{
	if(OpenDialog1->Execute())
	{
		try {
			TDocument * Document = new TDocument(PageControl1);
			PageControl1->ActivePageIndex = PageControl1->PageCount - 1;
			Document->Memo->Lines->LoadFromFile(OpenDialog1->FileName);
			Document->FileName = OpenDialog1->FileName;
			Document->Caption = OpenDialog1->FileName;
		} catch (...) {
			ShowMessage("Nie mo�na otworzy� pliku");
		}
	}
}

//---------------------------------------------------------------------------

void __fastcall TForm1::ToolButton3Click(TObject *Sender)
{
	Form1->Otwrz1Click(Sender);
}

//---------------------------------------------------------------------------

void __fastcall TForm1::ZapiszClick(TObject *Sender)
{
	TDocument * Document;
	Document = dynamic_cast<TDocument*>(PageControl1->Pages[PageControl1->ActivePageIndex]);

	if(Document->FileName != NULL && Document->FileName != "")
	{
		try {
			Document->Memo->Lines->SaveToFile(Document->FileName);
		} catch (...) {
			ShowMessage("Nie mo�na zapisa� pliku");
		}
	}
	else
	{
		 Form1->ZapiszJako1Click(MainMenu1);
    }
}

//---------------------------------------------------------------------------

void __fastcall TForm1::ToolButton2Click(TObject *Sender)
{
	Form1->ZapiszClick(Sender);
}

//---------------------------------------------------------------------------

void __fastcall TForm1::ZapiszJako1Click(TObject *Sender)
{
	TDocument * Document;
	Document = dynamic_cast<TDocument*>(PageControl1->Pages[PageControl1->ActivePageIndex]);

	if(SaveDialog1->Execute())
	{
		try {
			Document->Memo->Lines->SaveToFile(SaveDialog1->FileName);
			Document->FileName = SaveDialog1->FileName;
			Document->Caption = SaveDialog1->FileName;
		} catch (...) {
			ShowMessage("Nie mo�na zapisa� pliku");
		}
	}
}

//---------------------------------------------------------------------------
void __fastcall TForm1::Nowy1Click(TObject *Sender)
{
   	TDocument * Document = new TDocument(PageControl1);
	PageControl1->ActivePageIndex = PageControl1->PageCount - 1;
}

//---------------------------------------------------------------------------
void __fastcall TForm1::ToolButton1Click(TObject *Sender)
{
	Form1->Nowy1Click(Sender);
}

//---------------------------------------------------------------------------
void __fastcall TForm1::Zakocz1Click(TObject *Sender)
{
	Application->Terminate();
}
//---------------------------------------------------------------------------


void __fastcall TForm1::Zamknij1Click(TObject *Sender)
{
	if( PageControl1->PageCount == 1)
	{
		Application->Terminate();
	}
	else
	{
		PageControl1->ActivePage->Free();
	}
}

//---------------------------------------------------------------------------

void __fastcall TForm1::ToolButton4Click(TObject *Sender)
{
	Form1->Zamknij1Click(Sender);
}

//---------------------------------------------------------------------------

void __fastcall TForm1::Przelicz1Click(TObject *Sender)
{
	Form1->ToolButton5Click(Form1->MainMenu1);
}

//---------------------------------------------------------------------------

void __fastcall TForm1::ToolButton5Click(TObject *Sender)
{
	Form2->Memo1->Lines =  statystykaTekstu(PageControl1);
	Form2->Memo1->Lines->Add("\n");
	Form2->Memo1->Lines->AddStrings(statystykaWyrazow(PageControl1));
	Form2->ShowModal();
}

