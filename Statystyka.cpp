//---------------------------------------------------------------------------

#pragma hdrstop

#include "Statystyka.h"
#include "TDocument.h"
#include "Tokenize.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

void LiczZnaki(TStringList *slowa , TTablica &TablicaWynikowa )
{
	int i, dl;

	// najpierw zainicjujmy tablic� wynik�w
	for (i = 1; i< max_n_znakow; i++)
	{
		TablicaWynikowa[i] = 0;
	}

	// nast�pnie policzmy odpowiednie s�owa
	for (i=0; i<slowa->Count; i++)
	{
		dl = (*slowa)[i].Length();

		if (dl > max_n_znakow)
		{
			TablicaWynikowa[max_n_znakow-1] = TablicaWynikowa[max_n_znakow-1] + 1;
		}
		else if (dl > 0)
		{
			TablicaWynikowa[dl] = TablicaWynikowa[dl] + 1;
		}
	}
}

//---------------------------------------------------------------------------

void LiczZnakiMapBased(TStringList *slowa , std::map<int, int> &MapOfWords )
{
	int i, dl;

	for (i=0; i<slowa->Count; i++)
	{
		dl = (*slowa)[i].Length();

		if(MapOfWords.find(dl) != MapOfWords.end()) {
			MapOfWords[dl] =  MapOfWords[dl] + 1;
		}
		else
		{
			MapOfWords.insert(std::make_pair(dl, 1));
		}
	}
}

//---------------------------------------------------------------------------

UnicodeString merge(TStrings* Lines)
{
	String result = "";

	for(int i=0; i<Lines->Count; i++) {
		result += " " + Lines->Strings[i];
	}

	return result;
}

//---------------------------------------------------------------------------

int policzNiepuste(TStringList * StringList)
{
	int result = 0;

	for(int i=0; i<StringList->Count; i++) {
		if(StringList->Strings[i] != "")
		{
		   result ++;
        }
	}

	return result;
}

//---------------------------------------------------------------------------

TStringList *statystykaTekstu(TPageControl * PageControl)
{
	TDocument * Document;
	Document = dynamic_cast<TDocument*>(PageControl->ActivePage);

	int liczbaZnakow = 0;

	for(int i =0; i<Document->Memo->Lines->Count; i++) {
		UnicodeString LineText;
		LineText = Document->Memo->Lines->Strings[i];
		liczbaZnakow += LineText.Length();
	}

	int liczbaWyrazow = policzNiepuste(Tokenize(merge(Document->Memo->Lines), " "));
	int liczbaZdan = policzNiepuste(Tokenize(merge(Document->Memo->Lines), "."));
	int liczbaAkapitow = policzNiepuste(Tokenize(merge(Document->Memo->Lines), "\t"));

	TStringList *Result = new TStringList();

	Result->Add(
		"Podany tekst zawiera " +
		IntToStr(liczbaZnakow) +
		" znak(i|�w), " +
		IntToStr(liczbaWyrazow) +
		" wyraz(y|�w), " +
		IntToStr(liczbaZdan) +
		" zda�, " +
		IntToStr(liczbaAkapitow) +
		" akapit(y|�w), ");


	return Result;
}

//---------------------------------------------------------------------------

TStringList *statystykaWyrazow (TPageControl * PageControl)
{
	TDocument * Document;
	Document = dynamic_cast<TDocument*>(PageControl->ActivePage);

	TStringList *StringList = new TStringList();

	for(int i =0; i<Document->Memo->Lines->Count; i++) {
		StringList->AddStrings(Tokenize(Document->Memo->Lines->Strings[i], " "));
	}

	std::map<int, int> analizaWyrazow;

	LiczZnakiMapBased(StringList, analizaWyrazow);

	TStringList *Result = new TStringList();

	Result->Add("Statystyka wyraz�w:\n");

	std::map<int, int>::iterator it = analizaWyrazow.begin();
	while(it != analizaWyrazow.end())
	{
		Result->Add("Liczba wyraz�w o " + IntToStr(it->first) + " znakach: " + IntToStr(it->second));
		it++;
	}

	return Result;
}
//---------------------------------------------------------------------------