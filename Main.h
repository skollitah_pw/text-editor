//---------------------------------------------------------------------------

#ifndef MainH
#define MainH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.ComCtrls.hpp>
#include <System.ImageList.hpp>
#include <Vcl.ImgList.hpp>
#include <Vcl.ToolWin.hpp>
#include <Vcl.ActnCtrls.hpp>
#include <Vcl.ActnMan.hpp>
#include <Vcl.ExtCtrls.hpp>

//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TMainMenu *MainMenu1;
	TMenuItem *Zapisz;
	TMenuItem *Plik1;
	TMenuItem *Otwrz1;
	TMenuItem *Nowy1;
	TMenuItem *Zakocz1;
	TMenuItem *N1;
	TMenuItem *N2;
	TOpenDialog *OpenDialog1;
	TSaveDialog *SaveDialog1;
	TPageControl *PageControl1;
	TMenuItem *Zamknij1;
	TMenuItem *N3;
	TMenuItem *ZapiszJako1;
	TMenuItem *Statystyka1;
	TMenuItem *Przelicz1;
	TImageList *ImageList1;
	TToolBar *ToolBar1;
	TToolButton *ToolButton1;
	TToolButton *ToolButton2;
	TToolButton *ToolButton3;
	TToolButton *ToolButton4;
	TToolButton *ToolButton5;
	void __fastcall Otwrz1Click(TObject *Sender);
	void __fastcall ZapiszClick(TObject *Sender);
	void __fastcall Nowy1Click(TObject *Sender);
	void __fastcall Zakocz1Click(TObject *Sender);
	void __fastcall ZapiszJako1Click(TObject *Sender);
	void __fastcall Zamknij1Click(TObject *Sender);
	void __fastcall Przelicz1Click(TObject *Sender);
	void __fastcall ToolButton1Click(TObject *Sender);
	void __fastcall ToolButton2Click(TObject *Sender);
	void __fastcall ToolButton3Click(TObject *Sender);
	void __fastcall ToolButton4Click(TObject *Sender);
	void __fastcall ToolButton5Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
