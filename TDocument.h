//---------------------------------------------------------------------------

#ifndef TDocumentH
#define TDocumentH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.ComCtrls.hpp>

class TDocument : public TTabSheet
{
	public:
		TDocument(TPageControl *APageControl);
		__fastcall ~TDocument();
		void LoadFromFile(String AFileName);
		TMemo *Memo;
		String FileName;
};

#endif
