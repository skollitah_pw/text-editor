//---------------------------------------------------------------------------

#pragma hdrstop

#include "TDocument.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

TDocument::TDocument(TPageControl *APageControl): TTabSheet(APageControl)
{
	PageControl = APageControl;
	Caption = AnsiString("Nowy") + IntToStr(APageControl->PageCount);
	Memo = new TMemo(this);
	Memo->Parent = this;
	Memo->Align = alClient;
	Memo->WordWrap = false;
	Memo->ScrollBars = ssBoth;
	Memo->Lines->Clear();
}


TDocument::~TDocument() {
}

void TDocument::LoadFromFile(String AFileName)
{
	FileName = AFileName;
	Memo->Lines->LoadFromFile(FileName);
	Caption = ExtractFileName(FileName);
}