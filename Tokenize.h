//---------------------------------------------------------------------------

#ifndef TokenizeH
#define TokenizeH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>

TStringList *Tokenize (UnicodeString s,String delimiter);

#endif
