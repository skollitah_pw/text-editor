//---------------------------------------------------------------------------

#ifndef StatystykaH
#define StatystykaH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.ComCtrls.hpp>
#include <System.ImageList.hpp>
#include <Vcl.ImgList.hpp>
#include <Vcl.ToolWin.hpp>
#include <Vcl.ActnCtrls.hpp>
#include <Vcl.ActnMan.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <map>

#define max_n_znakow 20
typedef int TTablica[max_n_znakow];
void LiczZnaki(TStringList *slowa , TTablica &TablicaWynikowa);
void LiczZnakiMapBased(TStringList *slowa , std::map<int, int> &MapOfWords );
TStringList *statystykaTekstu(TPageControl * PageControl);
TStringList *statystykaWyrazow (TPageControl * PageControl);

#endif
