//---------------------------------------------------------------------------

#pragma hdrstop

#include "Tokenize.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)


TStringList *Tokenize (UnicodeString s,String delimiter)
{
	TStringList *StringList;
	int l;
	int start,delimp;
	String sub;
	StringList = new TStringList();
	start=0;

	if (s.Pos(delimiter)>0)
	{
		delimp = s.Pos(delimiter);
		do
		{
			l=s.Length();
			sub=s.SubString(start,delimp-1);

			if (sub != "") {
				StringList->Add(sub);
			}

			s=s.SubString(delimp+1,l-delimp);
			delimp = s.Pos(delimiter);
		} while (delimp != 0);

		StringList->Add(s);
	}
	else
	{
		StringList->Add(s);
	}

	return StringList;
}
